'''
Created on Feb 13, 2019

@author: alex
'''

from time import sleep
from labs.module05 import TempSensorAdaptor
#Instantiate TempSeensorAdaptor
tempSensorAdaptor = TempSensorAdaptor.TempSensorAdaptor()
tempSensorAdaptor.enableEmulator = True
print("Starting temp adaptor app")
#Run temsensoradaptor
tempSensorAdaptor.start()
while (True):
    sleep(5)
    pass