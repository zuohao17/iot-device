'''
Created on Mar 6, 2019

@author: alex
'''

import paho.mqtt.client as mqttClient
from labs.module06.SensordataInstance import SensordataInstance

#initiate a mqttclient
mqttclient = mqttClient.Client() 

#build a connection to subscribe the topic Assignment6
def on_connect(_Connection, _Data, _Token, resultCode):
 
    print("Client connected to server. Result: " + str(resultCode))
    # NOTE: This subscribes to ALL topics (depending on broker implementation)
    _Connection.subscribe("Assignment6")
 
#get the message from payload and transfer data between Json and sensordata
def on_message(clientConn, data, msg):
 
    print("Received PUBLISH on topic {0}. Payload: {1}".format(str(msg.topic), str(msg.payload)))
    strr = str(msg.payload)
    strr.lstrip("b");
    payload = strr.lstrip("'").rstrip("'");
    print("Received message:"+payload)
    SensordataInstance.Instance(payload);
    mqttclient.unsubscribe(msg.topic);
    
#run function
def run():
    mqttclient.on_connect = on_connect
    mqttclient.on_message = on_message
    mqttclient.connect("test.mosquitto.org", 1883, 60)
    mqttclient.loop_forever()


if __name__ == '__main__':  # pragma: no cover
    run()
