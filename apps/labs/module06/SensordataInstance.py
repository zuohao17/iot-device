'''
Created on Mar 7, 2019

@author: alex
'''

from labbenchstudios.common.DataUtil import DataUtil

class SensordataInstance(object):
    '''
    classdocs
    '''

    def __init__(self, params):
        '''
        Constructor
        '''
        #Build a instance to transfer data between Json and SensorData
    def Instance(self,data):
        dataUtil = DataUtil();
        DataU=dataUtil.jsonToSensorData(data)        # transfer JSON into sensorData instance
        print("Transfer Json to SensorData instance:")
        print(DataU) 
        print("Transfer SensorData instance to JSon:") # transfer SensorData instance to JSON
        dataUtil.toJson(DataU.name, 
                        DataU.timeStamp, 
                        DataU.avgValue, 
                        DataU.minValue, 
                        DataU.maxValue, 
                        DataU.curValue, 
                        DataU.totValue, 
                        DataU.sampleCount)  
