import psutil
from time import sleep
from threading import Thread 

class SystemPerformanceAdaptor(Thread):
    
    RATE_IN_SEC=10 
    # class init 
    def __init__(self, rateInSec=RATE_IN_SEC):
        super(SystemPerformanceAdaptor,self).__init__()
        self.rateInSec = rateInSec


    def run(self):
        while True:
            if self.enableAdaptor:
                print('\n--------------------')
                print('New system performance readings:')
                print('  ' + str(psutil.cpu_stats()))
                print('  ' + str(psutil.virtual_memory()))
            
            sleep(self.rateInSec)