import threading
from time import sleep
from labbenchstudios.common import SensorData
from labs.module02.SmtpClientConnector import SmtpClientConnector
from sense_hat import SenseHat
from labs.module03.TempActuatorEmulator import TempActuatorEmulator
from labbenchstudios.common import ActuatorData
from random import uniform



class TempSensorAdaptor(threading.Thread):
    
    #init parameters
    RATE_IN_SEC   = 10
    lowVal        = 0
    highVal       = 30
    isPrevTempSet = False
    alertDiff     = 2
    
    
    #create object
    sensorData   = SensorData.SensorData()
    connector    = SmtpClientConnector()
    sensehat     = SenseHat()
    tempactuator = TempActuatorEmulator()
    actuatordata = ActuatorData.ActuatorData()
    
    #init actuatordata
    actuatordata.setValue(0)
    actuatordata.setCommand(0)
    actuatordata.setErrorCode(0)
    actuatordata.setStateData(0)

    #init thread    
    def __init__(self, rateInSec = RATE_IN_SEC):
        super(TempSensorAdaptor, self).__init__()
        self.rateInSec = rateInSec
        
    #run function        
    def run(self):
        while True:
            if self.enableEmulator:
                self.curTemp = uniform(float(self.lowVal),float(self.highVal))
                self.sensorData.addValue(self.curTemp)
                
                print('\n--------------------')
                print('New sensor readings:')
                print(' ' + str(self.sensorData))
                
                #if isPrevTempSet is false, turn it into true 
                if self.isPrevTempSet == False:
                    self.prevTemp = self.curTemp
                    self.isPrevTempSet = True
                else:
                    #chect the different temperature with normal temperature
                    self.difference = self.tempactuator.check(self.curTemp)
                    
                    if (self.difference > 0):
                        self.tempactuator.setMessage(self.actuatordata, 0, 0, None, 1)
                        self.actuatordata.setValue(abs(self.difference))
                        self.tempactuator.processMessage(self.actuatordata)
                    
                    if (self.difference < 0):
                        self.tempactuator.setMessage(self.actuatordata, 1, 0, None, 1)
                        self.actuatordata.setValue(abs(self.difference))
                        self.tempactuator.processMessage(self.actuatordata)
                         
                    if (self.difference == 0):
                        print("Nothing to change")   
                    
                    if (abs(self.curTemp - self.sensorData.getAvgValue()) >= self.alertDiff):
                        print('\n Current temp exceeds average by > ' + str(self.alertDiff) + '. Triggering alert...')
                    self.connector.publishMessage('Exceptional sensor data [test] ', self.sensorData)
        
            sleep(self.rateInSec)
                
