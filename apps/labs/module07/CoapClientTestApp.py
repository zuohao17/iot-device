'''
Created on Mar 14, 2019

@author: alex
'''
import sys

from coapthon.client.helperclient import HelperClient
from coapthon.utils import parse_uri
from labs.common import SensorData
from labs.module06.SensordataInstance import SensordataInstance
from labs.common import ConfigConst

client=None

def usage():  # pragma: no cover
    print("Command:\tcoapclient.py -o -p [-P]")
    print("Options:")
    print("\t-o, --operation=\tGET|PUT|POST|DELETE|DISCOVER|OBSERVE")
    print("\t-p, --path=\t\t\tPath of the request")
    print("\t-P, --payload=\t\tPayload of the request")
    print("\t-f, --payload-file=\t\tFile with payload of the request")

#define callback method
def client_callback(response):
    print("Callback")

#call back observe method
def client_callback_observe(response):  # pragma: no cover
    global client
    print("Callback_observe")
    check = True
    while check:
        chosen = eval(input("Stop observing? [y/N]: "))
        if chosen != "" and not (chosen == "n" or chosen == "N" or chosen == "y" or chosen == "Y"):
            print("Unrecognized choose.")
            continue
        elif chosen == "y" or chosen == "Y":
            while True:
                rst = eval(input("Send RST message? [Y/n]: "))
                if rst != "" and not (rst == "n" or rst == "N" or rst == "y" or rst == "Y"):
                    print("Unrecognized choose.")
                    continue
                elif rst == "" or rst == "y" or rst == "Y":
                    client.cancel_observing(response, True)
                else:
                    client.cancel_observing(response, False)
                check = False
                break
        else:
            break
#choose method to achieve some function
def optionmodel(op,payload):
    global client
    path = "coap://localhost:5683"
    if op == "GET":
        if path is None:
            print("Path cannot be empty for a GET request")
            usage()
            sys.exit(2)
        response = client.get(path)
        print(response.pretty_print)
        client.stop()
    elif op == "DELETE":
        if path is None:
            print("Path cannot be empty for a DELETE request")
            usage()
            sys.exit(2)
        response = client.delete(path)
        print(response.pretty_print)
        client.stop()
    elif op == "POST":
        if path is None:
            print("Path cannot be empty for a POST request")
            usage()
            sys.exit(2)
        if payload is None:
            print("Payload cannot be empty for a POST request")
            usage()
            sys.exit(2)
        response = client.post(path, payload)
        print(response.pretty_print)
        client.stop()
    elif op == "PUT":
        if path is None:
            print("Path cannot be empty for a PUT request")
            usage()
            sys.exit(2)
        if payload is None:
            print("Payload cannot be empty for a PUT request")
            usage()
            sys.exit(2)
        response = client.put(path, payload)
        print(response.pretty_print)
        client.stop()
    else:
        print("Operation not recognized")
        usage()
        sys.exit(2)
     
#main function
def main():  # pragma: no cover
    global client
    sensorData = SensorData.SensorData()
    op1 = "GET"
    op2 = "PUT"
    op3 = "POST"
    op4 = "DELETE"
    path =" coap://0.0.0.0:5683"
    host = "0.0.0.0"
    port = 5683
    payload=str(sensorData)
    #get function by calling optionmodel
    optionmodel(op1,payload)
    optionmodel(op2,payload)
    optionmodel(op3,payload)
    optionmodel(op4,payload)
    SensordataInstance.Instance(payload)
    print("Payload data:"+payload)
    print("Successfully, payload data was recorded into data.json")
    client = HelperClient(server=(host, port));

if __name__ == '__main__':  # pragma: no cover
    main()