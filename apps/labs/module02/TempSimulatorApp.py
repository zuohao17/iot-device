from time import sleep
from labs.module02 import TempSensorEmulator
#activate the thread
tempSensorEmulator=TempSensorEmulator.TempSensorEmulator()
tempSensorEmulator.daemon=True
tempSensorEmulator.enableEmulator=True
print("Starting system performance app daemon thread...")
tempSensorEmulator.start()
while (True):
    sleep(10)
pass