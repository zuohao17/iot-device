from time import sleep
from threading import Thread
from random import uniform

from labbenchstudios.common import SensorData
from labs.module02 import SmtpClientConnector

class TempSensorEmulator(Thread):
    #init the temperature range
    lowVal=0
    highVal=30
    
    isPrevTempSet = False
    RATE_IN_SEC=10 
    #setup a potential threshold
    alertDiff = 5
    
    sensorData = SensorData.SensorData()
    connector = SmtpClientConnector.SmtpClientConnector()
   
    # class init 
    def __init__(self, rateInSec=RATE_IN_SEC):
        super(TempSensorEmulator,self).__init__()
        self.rateInSec = rateInSec
    #emulate the sensor data
    def run(self):
        while True:
            if self.enableEmulator:
                self.curTemp = uniform(float(self.lowVal), float(self.highVal))
                self.sensorData.addValue(self.curTemp)
                print('\n--------------------')
                print('New sensor readings:')
                print('  ' + str(self.sensorData))
                if self.isPrevTempSet == False:
                    self.prevTemp      = self.curTemp
                    self.isPrevTempSet = True
                else:
                    if (abs(self.curTemp - self.sensorData.getAvgValue()) >= self.alertDiff):
                        print('\n  Current temp exceeds average by > ' + str(self.alertDiff) + '. Triggering alert...')
                    self.connector.publishMessage('Exceptional sensor data [test]', self.sensorData)
            sleep(self.rateInSec)