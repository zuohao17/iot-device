'''import system file location'''
import sys # import sys local position
sys.path.append('/home/pi/workspace/iot-devices/apps')
from time import sleep #import sleep from time
from labs.module04 import I2CSenseHatAdaptor #import I2CSenseHatAdaptor from module04

'''activate the thread'''
i2cdata=I2CSenseHatAdaptor.I2CSenseHatAdaptor() #Instantiate I2CSenseHatAdaptor
i2cdata.daemon=True #initiate daemon 
i2cdata.enableEmulator=True #enable emulator
print("Starting system performance app daemon thread...") # print thread start
i2cdata.start()#thread start
while (True):# call sleep function
    sleep(10)
pass