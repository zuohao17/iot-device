'''
Created on Feb 9, 2019
@author: Hao Zuo
'''
'''import system file location'''
import sys
sys.path.append('/home/pi/workspace/iot-devices/apps') #import system file location
import smbus #import smbus
import threading #import thread function
from time import sleep #import sleep function from
from labbenchstudios.common import ConfigUtil # import Configutil from common file
from labbenchstudios.common import ConfigConst # import ConfigConst from common file

'''initiate i2cbus & get address from sensor'''
i2cBus = smbus.SMBus(1) # Use I2C bus No.1 on Raspberry Pi3 +
enableControl = 0x2D # control command
enableMeasure = 0x08 # enable measure command
accelAddr = 0x1C # address for IMU (accelerometer)
magAddr = 0x6A # address for IMU (magnetometer)
pressAddr = 0x5C # address for pressure sensor
humidAddr = 0x5F # address for humidity sensor
begAddr = 0x28 # setup starting reading address
totBytes = 6 # setup number of bytes
DEFAULT_RATE_IN_SEC = 5

class I2CSenseHatAdaptor(threading.Thread):
    rateInSec = DEFAULT_RATE_IN_SEC
    
    '''constructor'''
    def __init__(self):
        super(I2CSenseHatAdaptor, self).__init__()
        self.config = ConfigUtil.ConfigUtil(ConfigConst.DEFAULT_CONFIG_FILE_NAME)
        self.config.loadConfig() #loading config file
        print('Configuration data...\n' + str(self.config)) #print 
        self.initI2CBus() #initiate the I2Cbus
        
    '''define I2CBus function'''
    def initI2CBus(self):
        print("Initializing I2C bus and enabling I2C addresses...")
        i2cBus.write_quick(accelAddr) #write accelAddr address into i2cbus
        i2cBus.write_quick(magAddr) #write magAddr address into i2cbus
        i2cBus.write_quick(pressAddr)#write pressAddr address into i2cbus
        i2cBus.write_quick(humidAddr)#write humidAddr address into i2cbus
        
    '''define display function'''
    def displayAccelerometerData(self):
        data = i2cBus.read_i2c_block_data(accelAddr,begAddr,totBytes) #extract data from sensor by i2c and save into data
        print(' \n Accelerometer block data : ' + str(data))# print data
        
    def displayMagnetometerData(self):
        data = i2cBus.read_i2c_block_data(magAddr,begAddr,totBytes)#extract data from sensor by i2c and save into data
        print(' \n Magnetometer block data : ' + str(data)) # print data
        
    def displayPressureData(self):
        data = i2cBus.read_i2c_block_data(pressAddr,begAddr,totBytes)#extract data from sensor by i2c and save into data
        print(' \n Pressure block data : ' + str(data)) # print data
        
    def displayHumidityData(self):
        data = i2cBus.read_i2c_block_data(humidAddr,begAddr,totBytes) #extract data from sensor by i2c and save into data
        print(' \n Humidity block data : ' + str(data)) # print data
    
    '''run function'''
    def run(self):
        while True:
            if self.enableEmulator:# NOTE: you must implement these methods
                self.displayAccelerometerData()# call displayAccelerometerData function
                self.displayMagnetometerData() # call displayMagnetometerData function
                self.displayPressureData() # call displayPressureData function
                self.displayHumidityData() # call displayHumidityData function
            sleep(self.rateInSec) #call sleep function
